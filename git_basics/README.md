# Git basics from bash
Git is a tool that helps us to organize our work, keep track of it and the most important, it helps us to work consistently as a team.
Git uses what we call repositories, which is such as a regular folder, but with some hidden files that track all changes that we do to the repository.
Usually, we want to put the reposiories on the internet, so we can share those with our teammates and work together in many ways.
Here I will only show the basics, so you can start playing a little bit. More abour `git` and `Gitlab` will be discussed during the course.

To install git on linux, open the `Power Shell` as administrator and run:
```
choco install git
```
## Minor bash trick
`Ctrl+r` is a very useful command that permits you start typing and see the commands that you typed before that contain the new typed keys.

<img src="img/term_trick.PNG" width="800"/>

## Clone the repository locally
Clone the repository is the starting point of any project.

<img src="img/git_1.PNG" width="800"/>

## Add changes 
Add changes so git keeps track of that locally.

<img src="img/git_2.PNG" width="800"/>

## Commit and Push changes
Commit changes to your local repo. That changes will be updated to the online repo with `git push`.

<img src="img/git_3.PNG" width="800"/>

Also, to update your local repository with the last changes on the online repo, you want to use `git pull`.
