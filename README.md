# Data Analysis in PYthon V2

Hello and welcome to the DAPY course! I'm José María Lago, and I will be teaching you during this UAB/SEA course.

**WARNING: What you see in this repository may and would probably change till the last week before the course. Thus, you can start reading the content at your own risk.**

## What is this course about?
This course is about learning the basics of Data Analysis in Python. Data Analysis is a very interdisciplinary field, that moves from knowing some basics and tools of computer engineering, to statistical techniques to deal with complex datasets, to mathematical algorithms to solve the problems, to write nice reports with their corresponding beautiful plots.

<img src="https://upload.wikimedia.org/wikipedia/commons/e/ee/Relationship_of_data%2C_information_and_intelligence.png" width="500"/>

This course tries to show you the basics of that, so it will be an interdisciplinary course where you will learn the basics to start your own path as a Python Data Scientist.


### Real World
Usually, big companies have their own Data Science department, which tends to work with the following scheme:

<img src="img/work-diagram.png" width="800"/>

And more or less, the proportions of each type of worker are similar to those below:

<img src="img/DA-department.png" width="800"/>

### Bazaar vs Cathedral
**Linus's Law: "given enough eyeballs, all bugs are shallow".**

<img src="img/pythonvsothers.png" width="800"/>

In the Bazaar, as the source code is available for everyone, it admits public testing, scrutiny, and experimentation. Thus, all forms of bugs will be discovered faster. In contrast, an inordinate amount of time and energy must be spent hunting for bugs in the Cathedral model, since the working version of the code is available only to a few developers.

### Scheme of the course
Generally, during your education you have been taught things with a methodology that we could call "bottom up". Due to the time constraints of the course, we will use the "bottom up" methodology during the first day, and after that, the content will be taught "top down".

* **Day 1: (Coding)**
    - Reading the repo
    - Setting up your machines
    - Git / Gitlab
    - Python package managers
    - Jupyter-Notebook
    - Plain python
    - Basic DA libraries
* **Day 2: (Top view, DA and basic ML models)**
    - Top view on DA using python (toy examples)
    - DA philosophy
    - Greedy algorithms 
    - Decision Trees
* **Day 3: (RF from scratch)**
    - More DA concepts
    - Deep analysis on RF
    - Feature importance and PDP
    - Bring your own examples
* **Day 4: (GBM and Ensembling)**
    - Even more DA concepts from `slack`
    - Gradient Descent and Boosting
    - GBMs and Boosting Trees
    - `xgboost/lightgbm`
    - Big ensembling
    - Brief introduction to NNs (if we have time)

## Prerequisites
There is a fundamental prerequisite that make the others count near 0% combined. This is the attitude, to follow this course you only need to be positive, and have the intention to learn.
Being that said, below you can find the boring requirements:
* Basic Linear Algebra and Calculus concepts such as:  matrix, norm, class of functions
* Basic Coding concepts such as: array, byte, string, for loop, repository, csv file
* Basic Probability and Statistics concepts such as: random variable, linear regression
### Advice
What is crucial for the proper functioning of this course is for students to test the code and try their own examples at home. As explained below, students can discuss their concerns through the `slack` channel, and the most interesting results will be adapted and posted as `snippets` of the course.
## Before the course
As the course is limited by time, and by the internet speed of the classroom, which may surprise you (we hope not), there are some little steps that you can accomplish in your free time.
### Installation
Look at the `INSTALL` folder for a detailed guide. 
### Slack
Start playing with a tool called `slack`. Some days or weeks before the course, you will receive an e-mail from the SEA, with your credentials to join the `dapy` slack channel, which will be our communication tool during the whole course.

## Gitlab Snippets
During the course, we will use `Gitlab Snippets` as a forum where to put the main or most interesting ideas developed by the students. This will help your classmates to have a better understanding about the contents of the course. You can use the `slack` channel as a previous step to poll your classmates.