# External Resources for the course
This course attemps to show some tricks of data analysis. The following books are exhaustive books to learn the topics from the very basics till a good level of knowledge.

There is tons of information about machine learning and data analysis on the internet, but those books are all-inclusive and easy to read.

## Python
If you want to have an exhaustive knowledge of DA in Python you can read the following book:

<img src="img/DA_python.png" width="800"/>

Is important to read the 2nd edition, since this is a programming book and the materials change too fast.

## Machine Learning / Deep Learning
This book shows from the very basics of mathematics to complex deep learning models. It is also an online book and free. It is also a very exhaustive way to go at the first steps.

<img src="img/DL_book.png" width="800"/>