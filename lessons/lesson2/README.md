# Lesson 2
Today we are going to learn broadly the following: 
* Part 1 (Top view)
    * What is Kaggle?
    * Top view of what can we do with `python`
* Part 2 (DA Basics)
    * The actual viewpoint of Python DA
    * Why do we use DA and why it is so related to ML
    * Basic DA concepts and problem definition
    * Greedy (and fundamental) algorithms from scratch
    * Decision Trees from scratch
    * `sklearn`
* First real data example
    * Apply what we have learned to a real problem i.e. **Descriptive statistics + Greedy algorithms**



With the top view, we will glimpse the power of `python` for DA. In Part 2 we will go deep into the topic to learn more specifically of what DA is, why is very related to ML, and the simplest ML models. For the latter, we will test what we have learned on a real dataset, to see what can we get. 