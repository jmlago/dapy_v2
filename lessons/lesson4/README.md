# Lesson 4
Today we are going to learn broadly the following: 
* Gradient Descent for ML
* Boosting as ensembling (from scratch)
* Gradient Boosting Machines (GBMs)
* Tips and tricks to improve results
* `sklearn` BM
* Results on our dataset
* `xgboost` and `lightgbm`
* Big ensemble

Gradient Descent is the root of many ML learning models, evenmore with GBMs and NNs. We will built our Boosting Trees from scratch during this lesson in order to understand how these models work. After that, we will use the sklearn version on our datasets. Thereafter, we will use `xgboost` and `lightgbm`, two cutting edge algorithms that achieve state-of-the-art results in many Kaggle competitions. Finally we will see a widely used technique, the big ensembles. 