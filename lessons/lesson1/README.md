# Lesson 1
Today we are going to learn broadly the following: 
* What is `Python 3.6` and basic `python` syntax
* Why do we use `Python` for DA
* What is `Jupyter` and how to survive there
* Basic concepts of DA libraries such as `numpy` and `pandas`
* Plotting libraries such as `matplotlib` `seaborn` and `plotly`

This will be our first step in learning Python DA. Mainly the lesson is oriented to coding more than to DA itself, try to test the code and do the exercises. The best rule that you can apply when you learn a new language is **practice!**