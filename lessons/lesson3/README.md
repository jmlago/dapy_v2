# Lesson 3
Today we are going to learn broadly the following: 
* Can we go further than DT?
* Ensembling and Bagging techniques
* Random Forests (RF) from scratch
* How RF perform on real datasets
* `sklearn` RF
* Feature Analysis and PDP
* Try all that you have learned with your own datasets

One of the most important models on ML are RF. Learning how do they work from scratch is very important before using prebuilt models of `sklearn` because otherwise you wont really know how to tune the parameters. After that, we will see feature analysis and PDP, in order to interpret the results that we get from the simulator. Lastly you will apply those new concepts to your particular examples so you will practice ML and `python`.