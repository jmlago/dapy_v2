# Installation guide for DAPY on Linux

**If you have Windows 8/10 and have finished the windows tutorial, you must follow the instructions as if you have Linux.**

For OSX users, you must change all `sudo apt-get` by `brew` package manager. For those that use other `Linux` distros that are not based on Debian, you must change also `sudo apt-get` by your package manager. The guide was tested on Ubuntu so if you are having problems with other OS, ask your classmates via `slack`.

Optional commands:
```
sudo apt-get update
sudo apt-get upgrade
```

## Installing Python 3.6 with anaconda
Open your favortie browser and go to:
```
https://repo.continuum.io/archive/
```
And look for the most suitable version that must be `Anaconda3-5.3.1` and probably one of the following:

```
https://repo.continuum.io/archive/Anaconda3-5.3.1-Linux-x86_64.sh
https://repo.continuum.io/archive/Anaconda3-5.3.1-MacOSX-x86_64.sh
```

Open the archive location and type `./Anaconda*.sh` or `sh Anaconda*.sh` or `bash Anaconda*.sh` and follow the installation instructions (install anaconda in your home directory `~`). 
## Launch Jupyter-noebook
Go to a folder where you want to launch python and open a terminal to type the following:
```
jupyter-notebook
```

After that, `jupyter` will open a new tab on your default browser.

**If you are on windows, you must copy and paste the link where jupyter opens onto your favorite browser.**

**If you already have your Python 3.6 setting prior to this course, you just have to install jupyter-notebook with pip or conda**