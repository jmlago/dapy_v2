# Installation guide for DAPY
The installation process must be splitted in two types:
* Guide for `Linux` and `OSX` systems can be found inside  `Linux_and_OSX` folder.
* Guide for `Windows` users. Tests were done in `Windows 10` but is supposed to work in `Windows 7+`. You can find the guide inside `Windows` folder.