# Installation guide for DAPY in Windows
The guide was only tested for `Windows 8/10`.
## Windows 7
If you already know `cygwin`, install it and follow the Linux guide. Else you can download and install Anaconda natively on windows from the following URL:
```
https://repo.continuum.io/archive/Anaconda3-5.3.1-Windows-x86_64.exe
```
And follow the install instructions.

## Windows 8/10
This installation process was done using `Windows 10`, it is almost the same in `Windows 8`. 

## Microsoft Store
Install Ubuntu Application from Canonical.

<img src="../img/win_store_ubuntu.PNG" width="800"/>

## Activate Windows Subsystem for Linux

<img src="../img/changing_win_settings_for_ubuntu.PNG" width="800"/>

After that you must reset your computer.

## Open Ubuntu
Open Ubuntu App and a terminal will appear:

<img src="../img/ubuntu_shell_on_windows.PNG" width="800"/>

And you have now an Ubuntu inside Windows, with all terminal commands and properties of ubuntu.

**After that, you can go to the Linux installation guide, and follow there.**